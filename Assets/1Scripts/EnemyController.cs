﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

	GameObject Target;

	Rigidbody2D rigid;

	public EnemyModel enemyModel;

	public int Health;
	public float EnemySpeed;

	private GameManager GM;
	// Use this for initialization
	void Start() {
		GM = GameManager.Instance;
		Target = PlayerController.Instance.gameObject;
		rigid = GetComponent<Rigidbody2D>();
		Health = enemyModel.Health;
		EnemySpeed = enemyModel.Speed;
		rigid.mass = enemyModel.Mass; 
	}
	
	// Update is called once per frame
	void Update () {
		if(Target != null && Time.timeScale>0)
			rigid.velocity = ((Vector2)(Target.transform.position - transform.position).normalized);
		//Rigidbody.AddForce((Target.transform.position - transform.position).normalized,ForceMode2D.Impulse);
	}
	 void OnCollisionEnter2D(Collision2D other)
	 {
		 if(other.gameObject.tag == "Bullet")
		 {
			 float Damage = other.gameObject.GetComponent<Bullet>().Damage;
			 Health -= (int)Damage;
			 var sr = GetComponent<SpriteRenderer>();
			 StartCoroutine(Hit(sr));
			 CheckDeath();
		 }
		 if(other.gameObject.tag == "Player")
		 	PlayerController.Instance.LoseLife(enemyModel.Damage);
		
	
	 }
	 void CheckDeath()
	 {
		 if(Health < 1)
		 {
			 GM.Score++;
			 GM.UIM.UpdateScore();
			 Destroy(this.gameObject);
			//  GM.SpawnEnemy();
		 }
	 }

	public IEnumerator Hit(SpriteRenderer a)
	{
		a.color = Color.red;
		yield return new WaitForSeconds(0.1f);
		a.color = Color.white;
		yield return null;
	}
}
