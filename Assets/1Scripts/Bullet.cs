﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float Damage;
    private Rigidbody2D rigidBody;

    private PlayerController PC;
    private GameManager GM;
    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        GM = GameManager.Instance;
        PC = PlayerController.Instance;
        Damage = PC.CurrentWeapon.Damage;
    }

    // void OnTriggerEnter2D(Collider2D other)
    // {
    //     if (other.tag == "Border")
    //     {
    //         Vector2 teleportVector =GM.Teleport(other.gameObject);
    //         this.transform.Translate(teleportVector, Space.World);
    //     }
    //     // if(other.tag == "Bullet"){
    //     // 	// this.transform.position = Vector2.Reflect(other.transform.position, Vector2.left);
    //     // 	Vector2 inDirection = rigidBody.velocity;
    //     // 	Vector2 inNormal = other.GetComponent<Rigidbody2D>().velocity.normalized;
    //     // 	Vector2 newVelocity = Vector2.Reflect(inDirection, -inNormal);
    //     // 	rigidBody.AddForce(newVelocity*GM.ShotForce);
    //     // 	// Destroy(this.gameObject);
    //     // }
    // }

    void OnCollisionEnter2D(Collision2D other)
    {
        Debug.Log("asdddddddddddddd");
        if (other.gameObject.tag == "Player")
        {
            PC.LoseLife(Damage);
            Destroy(this.gameObject);
        }
        // if (other.gameObject.tag == "Bullet" || other.gameObject.tag == "Enemy")
        // {
        // }
        // if (other.gameObject.tag == "Border")
        // {
        //     // Vector2 teleportVector = GM.Teleport(other.gameObject);
        //     // Debug.Log("asdasd");
        //     // this.transform.Translate(teleportVector, Space.World);
        // }
        else if(other.gameObject.tag == "Enemy")
        {   
            int Health = other.gameObject.GetComponent<EnemyController>().Health;
            if(Health<Damage)
            {
                Damage -= Health;
            }
            else
                Destroy(this.gameObject);

        }
        else
           transform.rotation = Quaternion.Euler(new Vector3(0, 0, Mathf.Atan2(rigidBody.velocity.y, rigidBody.velocity.x) * Mathf.Rad2Deg));
    }

}
