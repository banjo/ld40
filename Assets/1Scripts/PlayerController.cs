﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public float Damage;
    public float Health;
    public int Ammo;
    public WeaponModel CurrentWeapon;
    private int weaponIndex = -1;
    // public float 
    private Rigidbody2D PlayerRB;
    private Animator animator;
    private SpriteRenderer PlayerSprite;
    private AudioSource audioSource;
    private float lastShootTime = -Mathf.Infinity;
    private GameManager GM;

    public List<AudioClip> winSounds;
    public List<AudioClip> deathSounds;
    public List<AudioClip> painSounds;

    public static PlayerController Instance;


    // Update is called once per frame
    void Awake()
    {
        Instance = this;
    }

    void Start(){
        GM = GameManager.Instance;
        PlayerRB = GetComponent<Rigidbody2D>();
        audioSource = GetComponent<AudioSource>();
        animator = this.transform.GetChild(0).GetComponent<Animator>();
        PlayerSprite = GetComponent<SpriteRenderer>();

        UnityStandardAssets.Utility.FollowTarget fT = Camera.main.gameObject.AddComponent<UnityStandardAssets.Utility.FollowTarget>();
        fT.target = this.transform;
        fT.offset = Vector3.back;

        ChangeWeapon();
        Ammo = CurrentWeapon.Ammo;
        UIManager.Instance.UpdateAmmo(Ammo,CurrentWeapon.Ammo);
        

        DetectTouchArea.OnAreaTouch += Move;
    }

    /// <summary>
    /// This function is called when the MonoBehaviour will be destroyed.
    /// </summary>
    void OnDestroy()
    {
        DetectTouchArea.OnAreaTouch -= Move;
    }

    void Move()
    {
        Vector3 dir = (Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position));
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg));

        float t=0;
        if(Ammo <= 0)
            t = CurrentWeapon.ReloadTime;
        else 
            t = CurrentWeapon.FireRate;
        
        if (lastShootTime + t < Time.time)
        {
            lastShootTime = Time.time;
            if(Ammo == 0)
            {
                //Reload
                Debug.Log("Reload");
                Ammo = CurrentWeapon.Ammo;
                RandomSound(CurrentWeapon.reloadSounds);
            }
            else{
                Shoot(dir);
                RandomSound(CurrentWeapon.fireSounds);
            }
            UIManager.Instance.UpdateAmmo(Ammo,CurrentWeapon.Ammo);
        }
        
    }

    void Shoot(Vector2 dir)
    {
        Vector3 Force = dir.normalized * CurrentWeapon.Force;
        Vector3 randVec;
        
        for (int i = 0; i < CurrentWeapon.BulletShotCount; i++)
        {
            if(CurrentWeapon._name == "Pistol")
             randVec = transform.position + (Vector3)dir.normalized;
            else
             randVec = (Vector3)Random.insideUnitCircle + transform.position + (Vector3)dir.normalized * 2;
            GameObject Bullet = Instantiate(CurrentWeapon.BulletPrefab, randVec, transform.rotation, GM.LevelContainer);
            Rigidbody2D BulletRB = Bullet.GetComponent<Rigidbody2D>();
            BulletRB.mass = CurrentWeapon.BulletMass;
            BulletRB.AddForce(Force/CurrentWeapon.BulletShotCount, ForceMode2D.Impulse);

            Bullet bulletController = Bullet.GetComponent<Bullet>();
            bulletController.Damage = CurrentWeapon.Damage;
        }
        GM.UpdateScore(-1);
        PlayerRB.AddForce(-Force*PlayerRB.mass,ForceMode2D.Impulse);
        Ammo--;
        animator.SetTrigger("Shot");
    }

    public void ChangeWeapon(){
        List<LevelWeapon> weapons = GM.CurrentLevel.Weapons;
        if(weapons == null || weapons.Count == 0)return;
        while(true){
            weaponIndex = (weaponIndex + 1)%weapons.Count;
            Debug.Log(weaponIndex);
            if(!weapons[weaponIndex].Locked)
                break;
        }
        CurrentWeapon = weapons[weaponIndex].model;
        Ammo = CurrentWeapon.Ammo;
        UIManager.Instance.UpdateAmmo(Ammo,Ammo);
        animator.runtimeAnimatorController = CurrentWeapon.animatorController;
        Debug.Log(CurrentWeapon._name);

    }


    public void LoseLife(float Damage)
    {
        GM.UpdateScore(-1);
        Health -= Damage;
//        Debug.LogError(Health);

        StartCoroutine(Hit());
        // UIManager.Instance.Health.text = "Health:" + Health;
        // StartCoroutine("Invincibility");
        UIManager.Instance.UpdateHealth(Health);
        if (Health <= 0)
        {
            GM.UpdateScore(-50);
            GM.GameOver();
            DeathSound();
            return;
        }
        RandomSound(painSounds);

    }

    public IEnumerator Hit()
    {
        PlayerSprite.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        PlayerSprite.color = Color.white;
        yield return null;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Finish")
        {
            GM.Win();
            DetectTouchArea.OnAreaTouch -= Move;
            WinSound();
            
        }
        if (other.gameObject.tag == "Item")
        {
            Debug.LogError("naniii");
            other.GetComponent<Pickup>().Picked();
        }

        // if (other.gameObject.tag == "Border")
        // {
        //     LoseLife(100);
        //     RandomSound(painSounds);          
        //     DetectTouchArea.OnAreaTouch -= Move;
            
        // }
    }

    private void WinSound(){
        RandomSound(winSounds);
    }

    private void DeathSound(){
        RandomSound(deathSounds);
    }

    private void RandomSound(List<AudioClip> lst){
        int r = Random.Range(0, lst.Count);
        audioSource.PlayOneShot(lst[r]);        
    }

}
