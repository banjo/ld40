﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletEmitter : MonoBehaviour {

	public List<GameObject> BulletPrefabs;

	public int Count;
	public float FireRate;

	public Vector2 RandomForce;

	IEnumerator Start(){
		int i = 0;
		while(i < Count){
			Vector2 randVec = Random.insideUnitCircle;
			Vector2 dir = Random.insideUnitCircle.normalized;
        	Quaternion rot = Quaternion.Euler(new Vector3(0, 0, Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg));

			int r = Random.Range(0, BulletPrefabs.Count);
			GameObject Bullet = Instantiate(BulletPrefabs[r], this.transform.position + (Vector3)randVec, rot);
            Rigidbody2D BulletRB = Bullet.GetComponent<Rigidbody2D>();
            BulletRB.AddForce(randVec*Random.Range(RandomForce.x, RandomForce.y), ForceMode2D.Impulse);

			yield return new WaitForSeconds(FireRate);
			i++;
		}
	}
}
