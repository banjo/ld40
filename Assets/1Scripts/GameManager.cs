﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public int Score;


    public Transform LevelContainer;

    public float LevelStartTime;
    public float LevelEndTime;

    public float StartHp;
    public float EndHp;

    public List<LevelModel> Levels;
    int LevelIndex;

    public LevelModel CurrentLevel;


    public static GameManager Instance = null;
    internal UIManager UIM;




    // Use this for initialization
    void Awake()
    {
        Instance = this;
        StartHp = 100;
        EndHp = 100;
    }

    void Start()
    {
        UIM = UIManager.Instance;
        LevelIndex = -1;
        LoadNextLevel();

    }
    // Update is called once per frame

    public Vector2 Teleport(GameObject collision)
    {
        return CurrentLevel.Teleport(collision);
    }

    // IEnumerator Invincibility()
    // {
    //     Invulnerable = true;
    //     Player.GetComponent<SpriteRenderer>().color = Color.gray;
    //     yield return new WaitForSeconds(3);
    //     Player.GetComponent<SpriteRenderer>().color = Color.white;
    //     Invulnerable = false;
    //     yield return null;
    // }

    public void GameOver()
    {
        Time.timeScale = 0;
        UIM.GameOver();
    }

    public void ResetGameState()
    {
        Time.timeScale = 1;
        StopAllCoroutines();
        LevelContainer.DestroyChildren();
    }
    public void Win()
    {
        EndHp = PlayerController.Instance.Health;
        Time.timeScale = 0;
        LevelEndTime = Time.time;
        UpdateScore((int)(LevelStartTime - LevelEndTime));
        UIM.LevelComplete();
    }

    public void LoadNextLevel()
    {
        LevelStartTime = Time.time;
        ResetGameState();
        LevelIndex++;
        if (LevelIndex == Levels.Count)
            LevelIndex = 0;
        CurrentLevel = Levels[LevelIndex];
        CurrentLevel.InitLevel(LevelContainer);
        StartCoroutine(CurrentLevel.EnemySpawer());
        
        if (LevelIndex == 0)
            StartHp = PlayerController.Instance.Health;
        else{
            StartHp = EndHp;
            PlayerController.Instance.Health = EndHp;}
        Score += CurrentLevel.Score;
        UIM.UpdateHealth(EndHp);
    }

    public void UpdateScore(int sc)
    {
        Score += sc;
        if (Score <= 0)
            Score = 0;
        UIM.UpdateScore();
    }
    public void RestartLevel()
    {

        ResetGameState();
        LevelStartTime = Time.time;
        CurrentLevel = Levels[LevelIndex];
        // CurrentWeapon = CurrentLevel.Weapons[0];
        CurrentLevel.InitLevel(LevelContainer);
        PlayerController.Instance.Health = StartHp;
        StartCoroutine(CurrentLevel.EnemySpawer());
        // StartCoroutine(CurrentLevel.ItemSpawer());
    }
}
