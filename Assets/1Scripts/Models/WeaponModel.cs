﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponModel : Droppable
{

    public string _name;
    public Sprite icon;
	public RuntimeAnimatorController animatorController;
    // public Sprite sprite;
    public List<AudioClip> fireSounds;
    public List<AudioClip> reloadSounds;

    public int Ammo;
    public int BulletShotCount = 1;
    public float BulletMass;
    public float Damage;
    public float ReloadTime;
    public float FireRate;
    public float Force;

    public bool Purchased; // hah

    public GameObject BulletPrefab;


    public override void Effect()
    {
        // GameManager.Instance.PlayerHealth += HP;
        PlayerController.Instance.Ammo += Ammo;
        // GameManager.Instance.EnemySpeed += Speed;
    }



}
