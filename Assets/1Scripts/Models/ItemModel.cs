﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemModel : Droppable {

	public int HP;
	public float Damage;
	// public float Ammo;

	public override void Effect(){
		PlayerController.Instance.Health += HP;
		PlayerController.Instance.Damage += Damage;
		// GameManager.Instance.EnemySpeed += Speed;
	}
	
}
