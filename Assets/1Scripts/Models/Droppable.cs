﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Droppable : ScriptableObject {
    public float lifeTime;
    public GameObject Prefab;

    public virtual void Effect(){}
}
