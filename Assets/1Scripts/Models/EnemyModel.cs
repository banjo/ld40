﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyModel : ScriptableObject {

	public string _name;
	public int Health;
	public int Damage;
	public float Speed;
	public float Mass;
	public Sprite icon;
	public Sprite sprite;
	public GameObject Prefab; 
}
