﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct LevelWeapon
{
    public WeaponModel model;
	[System.NonSerialized]
    public bool Locked;
}


public class LevelModel : ScriptableObject
{

    public GameObject LevelPrefab;
    public GameObject PlayerPrefab;
    public GameObject FinishPrefab;

	public int Score;
    public List<LevelWeapon> Weapons;

    public List<ItemModel> Items;
    public List<EnemyModel> Enemies;

    private Vector2 MapSize;
    private GameObject LeftBorder;
    private GameObject RightBorder;
    private GameObject TopBorder;
    private GameObject BottomBorder;

    private bool IsPlaying = false;

    private GameManager GM;

    public void InitLevel(Transform parent)
    {

        GM = GameManager.Instance;

        IsPlaying = true;
        GameObject clone = Instantiate(LevelPrefab, parent);
        Transform borders = clone.transform.Find("Borders").transform;
        LeftBorder = GetTriggerBorder(borders.GetChild(0), borders);
        RightBorder = GetTriggerBorder(borders.GetChild(1), borders);
        TopBorder = GetTriggerBorder(borders.GetChild(2), borders);
        BottomBorder = GetTriggerBorder(borders.GetChild(3), borders);

        MapSize = new Vector2(RightBorder.transform.position.x - LeftBorder.transform.position.x - 1, TopBorder.transform.position.y - BottomBorder.transform.position.y - 1);
        // GM.Player = clone.transform.Find("Player").gameObject;
        Vector2 StartPosition = clone.transform.Find("Start").transform.position;
        Vector2 FinishPosition = clone.transform.Find("Finish").transform.position;
        Instantiate(PlayerPrefab, StartPosition, Quaternion.identity, clone.transform);
        Instantiate(FinishPrefab, FinishPosition, Quaternion.identity, clone.transform);
    }

    public Vector2 Teleport(GameObject collision)
    {
        Vector2 v = Vector2.zero;
        // Debug.Log(collision);
        if (collision.gameObject == LeftBorder)
            v = Vector2.right * MapSize.x;
        if (collision.gameObject == RightBorder)
            v = Vector2.left * MapSize.x;
        if (collision.gameObject == TopBorder)
            v = Vector2.down * MapSize.y;
        if (collision.gameObject == BottomBorder)
            v = Vector2.up * MapSize.y;
        return v;
    }

    private GameObject GetTriggerBorder(Transform border, Transform parent)
    {
        GameObject g = Instantiate(border.gameObject, parent);
        BoxCollider2D col = g.GetComponent<BoxCollider2D>();
        col.size = new Vector2(0.5f, border.GetComponent<SpriteRenderer>().size.y);
        col.isTrigger = true;
        return g;
    }




    public IEnumerator EnemySpawer()
    {
        if (Enemies == null || Enemies.Count == 0) yield break;
        while (IsPlaying)
        {
            EnemyModel em = Enemies[Random.Range(0, Enemies.Count)];

            var a = Instantiate(em.Prefab, (Vector3)Random.insideUnitCircle.normalized * Mathf.Min(MapSize.x, MapSize.y) + PlayerController.Instance.transform.position, Quaternion.identity,GM.LevelContainer);
            a.GetComponent<EnemyController>().enemyModel = em;
            yield return new WaitForSeconds(4);

        }
    }

    // public IEnumerator ItemSpawer()
    // {
    //     List<Droppable> droppables = new List<Droppable>();
    //     foreach (var v in Items)
    //         droppables.Add((Droppable)v);

    //     foreach (var w in Weapons)
    //         droppables.Add((Droppable)w.model);

    //     if (droppables.Count == 0) yield break;


    //     while (IsPlaying)
    //     {
    //         Droppable em = droppables[Random.Range(0, droppables.Count)];
    //         GameObject g = Instantiate(em.Prefab, new Vector3(Random.Range(-MapSize.x / 5, MapSize.x / 5), Random.Range(-MapSize.y / 5, MapSize.y / 5), 0), Quaternion.identity);
    //         g.AddComponent<Pickup>().droppable = em;
    //         yield return new WaitForSeconds(1);

    //     }
    // }

}
