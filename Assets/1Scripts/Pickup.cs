﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour {

	public Droppable droppable; 

	// Use this for initialization
	IEnumerator Start () {
		yield return new WaitForSeconds(droppable.lifeTime);
		Destroy(this.gameObject);
	}

	public void Picked(){
		droppable.Effect();
		Destroy(this.gameObject);
		Debug.Log("opaaa");

	}
	
	// // Update is called once per frame
	// void Update () {
		
	// }
}
