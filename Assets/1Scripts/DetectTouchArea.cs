﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DetectTouchArea : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

	public delegate void DetectTouch();
	public static event DetectTouch OnAreaTouch;

	bool IsTouching = false;


    /// <summary>
	/// Update is called every frame, if the MonoBehaviour is enabled.
	/// </summary>
	void Update()
	{
		if(IsTouching && OnAreaTouch != null)
			OnAreaTouch();
		
	}

    public void OnPointerDown(PointerEventData eventData)
    {
		IsTouching = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
		IsTouching = false;
    }
}
