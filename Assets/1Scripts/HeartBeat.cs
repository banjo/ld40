﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartBeat : MonoBehaviour {

	public Vector2 MinMax;
	public float speed;
	private float delay;

	/// <summary>
	/// Start is called on the frame when a script is enabled just before
	/// any of the Update methods is called the first time.
	/// </summary>
	void Awake()
	{
		delay = Random.Range(0, 100f);
	}
	
	// Update is called once per frame
	void Update () {

		float z=MinMax.x+Mathf.PingPong(delay+Time.time*speed, MinMax.y-MinMax.x);
		transform.localScale = Vector3.one * z;
	}
}