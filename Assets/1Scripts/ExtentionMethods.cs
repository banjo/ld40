﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class ExtentionMethods
{
	public static Z FindChildInHierarchy<Z>(this Transform trans, string name) where Z : Component
	{
		Z[] components = trans.GetComponentsInChildren<Z>(true);
		foreach (Z v in components)
		{
			if (v.name == name)
			{
				return v;
			}
		}
		return default(Z);
	}

	public static Z GetComponentInChildren<Z>(this GameObject trans, bool excl = false, bool inactive = false) where Z : Component
	{
		Z[] components = trans.GetComponentsInChildren<Z>(inactive);
		if (!excl) return components[0];
		return components[1];
	}

	public static Z[] GetChildrenComponents<Z>(this GameObject trans, bool excl = false, bool inactive = false) where Z : Component
	{
		Z[] components = trans.GetComponentsInChildren<Z>(inactive);
		if (!excl) return components;
		return components.Skip(1).ToArray();
	}

	public static void DestroyChildren(this Transform trans){
		foreach(Transform t in trans)
			GameObject.Destroy(t.gameObject);

	}


    private static int CountCornersVisibleFrom(this RectTransform rectTransform, Camera camera)
    {
        Rect screenBounds = new Rect(0f, 0f, Screen.width, Screen.height); // Screen space bounds (assumes camera renders across the entire screen)
        Vector3[] objectCorners = new Vector3[4];
        rectTransform.GetWorldCorners(objectCorners);
 
        int visibleCorners = 0;
        Vector3 tempScreenSpaceCorner; // Cached
        for (var i = 0; i < objectCorners.Length; i++) // For each corner in rectTransform
        {
            tempScreenSpaceCorner = camera.WorldToScreenPoint(objectCorners[i]); // Transform world space position of corner to screen space
            if (screenBounds.Contains(tempScreenSpaceCorner)) // If the corner is inside the screen
            {
                visibleCorners++;
            }
        }
        return visibleCorners;
    }
 

    public static bool IsFullyVisibleFrom(this RectTransform rectTransform, Camera camera)
    {
        return CountCornersVisibleFrom(rectTransform, camera) == 4; // True if all 4 corners are visible
    }

    public static bool IsVisibleFrom(this RectTransform rectTransform, Camera camera)
    {
        return CountCornersVisibleFrom(rectTransform, camera) > 0; // True if any corners are visible
    }

    public static string ToHex(this Color color)
     {
         string hex = ((int)(color.r*255)).ToString("X2") + ((int)(color.g*255)).ToString("X2") + ((int)(color.b*255)).ToString("X2");
         return "#"+hex;
     }

}