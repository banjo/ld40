﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

private GameManager GM;
public Text Score;
public Text Ammo;
public Text HealthText;
public Button ChangeButton;
public GameObject ShopWindow;
public GameObject GameOverWindow;
public GameObject WinWindow;
public Button ExitButton;
public Button RetryButton;
public Button RetryButton2;
public Button ShopButton;
public Button NextLevelButton;
public Image HealthBar;
public static UIManager Instance;


void Awake()
{
	Instance = this;
}
void Start()
{	
	GM = GameManager.Instance;
	ResetUI();
	//ShopButton.onClick.AddListener(()=>Time.timeScale = 1f);
	RetryButton.onClick.AddListener(()=>{GM.RestartLevel();ResetUI();});
	RetryButton2.onClick.AddListener(()=>{GM.RestartLevel();ResetUI();});
	NextLevelButton.onClick.AddListener(()=>{GM.LoadNextLevel();ResetUI();});
	ChangeButton.onClick.AddListener(ChangeWeaponButton);
	ExitButton.onClick.AddListener(()=>{Time.timeScale = 1;UnityEngine.SceneManagement.SceneManager.LoadScene(0);});
}

public void ResetUI()
{
	UpdateHealth(GM.StartHp);
	UpdateScore();
	GameOverWindow.SetActive(false);
	ShopWindow.SetActive(false);
	WinWindow.SetActive(false);

}

public void UpdateHealth(float Health)
{
	HealthBar.fillAmount = Health/100f;
	HealthText.text = Health+"";
}

public void UpdateScore()
{
	Score.text = "" + GM.Score;
}

public void UpdateAmmo(int cur, int max){
	Ammo.text = cur + "/" + max;
}

public void LevelComplete()
{
	WinWindow.transform.Find("Score").GetComponent<Text>().text = "Your Score:" + GM.Score;
	WinWindow.SetActive(true);
}
public void GameOver()
{
	GameOverWindow.transform.Find("Score").GetComponent<Text>().text = "Your Score:" + GM.Score;
	GameOverWindow.SetActive(true);
}
public void ChangeWeaponButton(){
	PlayerController.Instance.ChangeWeapon();
}


}
