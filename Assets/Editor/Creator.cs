﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Creator : MonoBehaviour {

	[MenuItem("Creator/Level")]
	public static void CreateLevel(){
		CreateAsset<LevelModel>("Levels/NewLevel");
	}

	[MenuItem("Creator/Weapon")]
	public static void CreateWeapon(){
		CreateAsset<WeaponModel>("Weapons/NewWeapon");
	}

	[MenuItem("Creator/PowerUp")]
	public static void CreatePowerUp(){
		CreateAsset<ItemModel>("PowerUps/NewPowerUp");
	}

	[MenuItem("Creator/Enemy")]
	public static void CreateEnemy(){
		CreateAsset<EnemyModel>("Enemies/NewEnemy");
	}

	private static void CreateAsset<T>(string path) where T : ScriptableObject{
		T asset = ScriptableObject.CreateInstance<T>();

		if(!AssetDatabase.IsValidFolder(path))
			AssetDatabase.CreateFolder(path.Split('/')[0],path.Split('/')[1]);

        AssetDatabase.CreateAsset(asset, "Assets/Resources/"+path+Random.Range(0,100)+".asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
	} 
}
